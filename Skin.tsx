import {
  Path as ISketchPath,
  SketchCanvas,
} from "@terrylinla/react-native-sketch-canvas";
import React, {
  FC,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  View,
  ViewStyle,
} from "react-native";
import { captureRef } from "react-native-view-shot";
import * as MediaLibrary from "expo-media-library";
import * as FileSystem from "expo-file-system";
import { hasPerms } from "./utils";
import Button from "./Button";
import Svg, { Path } from "react-native-svg";
import Draggable from "react-native-draggable";

type ISkinZoneName =
  | "head"
  | "throatLeft"
  | "neck"
  | "throatRight"
  | "shoulderLeft"
  | "thoraxLeft"
  | "sideLeft"
  | "legLeft"
  | "shoulderRight"
  | "thoraxRight"
  | "sideRight"
  | "legRight"
  | "back";

type IPinType = "perforation" | "consumption";

export type IPin = {
  type: IPinType;
  coords?: {
    top: number;
    left: number;
  };
  zone?: ISkinZoneName;
};

const Pin: FC<{
  pin: IPin;
  style?: StyleProp<ViewStyle>;
  pinStyle?: StyleProp<ViewStyle>;
}> = ({ pin, style, pinStyle }) => {
  //const coords = pin.coords || {};
  return (
    <View style={[styles.pinOuter, style]}>
      <View style={[styles.pin, pinStyle]}>
        <Text style={styles.pinChar}>{pin.type.charAt(0).toUpperCase()}</Text>
      </View>
    </View>
  );
};

type ISkin = {
  name?: string;
  paths?: ISketchPath[];
  onPathAdded?: (path: ISketchPath) => void;
  onPathsChange?: (paths: ISketchPath[]) => void;
  pins?: IPin[];
  newPinType?: IPinType;
  onPinsChange?: (pins: IPin[]) => void;
};

const Skin = forwardRef(
  (
    {
      name,
      paths,
      onPathAdded,
      onPathsChange,
      pins,
      newPinType,
      onPinsChange,
    }: ISkin,
    ref
  ) => {
    const containerToCapture = useRef(null);
    const sketch = useRef<SketchCanvas>(null);
    const [isDrawing, setIsDrawing] = useState(false);
    const [curTool, setCurTool] = useState<"pencil" | "eraser">("pencil");
    const [canRemovePin, setCanRemovePin] = useState(false);

    useImperativeHandle(ref, () => ({
      capture: (albumName: string) => {
        capture(albumName);
      },
    }));

    useEffect(() => {
      if (!sketch) {
        return;
      }
      sketch.current?.clear();
      paths?.map((path) => {
        sketch.current?.addPath(path);
      });
    }, [sketch, paths]);

    const capture = async (albumName: string) => {
      const tmp = await captureRef(containerToCapture, {
        result: "tmpfile",
        quality: 1,
        format: "jpg",
      });

      const dest = `${FileSystem.documentDirectory}${new Date().getTime()}.jpg`;
      await FileSystem.moveAsync({
        from: tmp,
        to: dest,
      });
      const { uri } = await FileSystem.getInfoAsync(dest);

      const hasPerm = await hasPerms();
      if (!hasPerm) {
        return;
      }

      const asset = await MediaLibrary.createAssetAsync(uri);
      let album = await MediaLibrary.getAlbumAsync(albumName);
      if (!album) {
        MediaLibrary.createAlbumAsync(albumName, asset, false);
      } else {
        MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
      }
    };

    const toggleDraw = () => {
      const nextIsDrawing = !isDrawing;
      setIsDrawing(nextIsDrawing);
      if (nextIsDrawing) {
        setCurTool("pencil");
      }
    };

    const onStrokeEnd = (path: ISketchPath) => {
      onPathAdded?.(path);
      const paths = sketch.current?.getPaths() || [];
      paths.push(path);
      onPathsChange?.(paths);
    };

    const undoSketch = () => {
      sketch.current?.undo();
      onPathsChange?.(sketch.current?.getPaths() || []);
    };

    const onSkinZonePress = (e: GestureResponderEvent, zone: ISkinZone) => {
      if (canRemovePin || !newPinType) {
        return;
      }
      const pin: IPin = {
        type: newPinType,
        zone: zone.name,
        coords: {
          top: e.nativeEvent.locationY,
          left: e.nativeEvent.locationX,
        },
      };
      pins = pins ? [...pins] : [];
      pins.push(pin);
      onPinsChange?.(pins);
    };

    const onPinMoved = () => {
      onPinsChange?.([...(pins as IPin[])]);
    };

    const onPinRelease = (pin: IPin) => {
      if (canRemovePin) {
        pins = pins ? [...pins] : [];
        pins.splice(pins.indexOf(pin), 1);
        onPinsChange?.(pins);
      }
    };

    return (
      <View style={styles.container}>
        <View ref={containerToCapture} style={styles.skinContainer}>
          <SkinSvg color="#CCC" />
          <SketchCanvas
            ref={sketch}
            user={name}
            touchEnabled={isDrawing}
            strokeColor={curTool == "eraser" ? "#00000000" : "red"}
            strokeWidth={curTool == "eraser" ? 20 : 7}
            style={styles.sketch}
            onStrokeEnd={onStrokeEnd}
          />
          {!isDrawing && (
            <SkinSvg
              color="transparent"
              onZonePress={(e, zone) => onSkinZonePress(e, zone)}
            />
          )}
          <View style={styles.pins} pointerEvents="box-none">
            {pins?.map((pin, index) => (
              <Draggable
                key={`${index}${pin.coords?.top}${pin.coords?.left}`}
                x={pin.coords?.left}
                y={pin.coords?.top}
                onRelease={() => onPinRelease(pin)}
                onDragRelease={(e, g) => {
                  pin.coords = {
                    left: (pin.coords?.left as number) + g.dx,
                    top: (pin.coords?.top as number) + g.dy,
                  };
                  onPinMoved();
                }}
              >
                <Pin pin={pin} />
              </Draggable>
            ))}
          </View>
        </View>
        <View style={styles.tools}>
          <View style={styles.toolsRow}>
            <Button
              label={!isDrawing ? "Dessiner" : "Arrêter"}
              onPress={() => toggleDraw()}
              buttonStyle={{
                backgroundColor: !isDrawing ? "#336699" : "#FF0000",
              }}
            />
            {isDrawing && (
              <View
                style={{
                  flexDirection: "row",
                }}
              >
                <View style={styles.tool}>
                  <Button
                    label="Crayon"
                    buttonStyle={{
                      backgroundColor: curTool == "pencil" ? "#336699" : "#CCC",
                    }}
                    onPress={() => setCurTool("pencil")}
                  />
                </View>
                <View style={styles.tool}>
                  <Button
                    label="Gomme"
                    buttonStyle={{
                      backgroundColor: curTool == "eraser" ? "#336699" : "#CCC",
                    }}
                    onPress={() => setCurTool("eraser")}
                  />
                </View>
                <View style={styles.tool}>
                  <Button
                    label="Annuler"
                    onPress={undoSketch}
                    buttonStyle={{
                      backgroundColor: "#CCC",
                    }}
                  />
                </View>
              </View>
            )}
          </View>
          <View style={styles.toolsRow}>
            <Button
              label={
                !canRemovePin
                  ? "Supprimer des marqueurs"
                  : "Arrêter la suppression de marqueurs"
              }
              buttonStyle={{
                backgroundColor: !canRemovePin ? "#336699" : "#FF0000",
              }}
              onPress={() => setCanRemovePin(!canRemovePin)}
            />
          </View>
        </View>
      </View>
    );
  }
);

const SkinSvg: FC<{
  color: string;
  onZonePress?: (e: GestureResponderEvent, zone: ISkinZone) => void;
}> = ({ color, onZonePress }) => {
  return (
    <Svg viewBox="0 0 406.77 571.98" style={styles.skin}>
      {svgZones.map((zone, index) => {
        return (
          <Path
            key={index}
            fill={color}
            d={zone.d}
            transform={zone.transform}
            onPressIn={(e) => {
              onZonePress?.(e, zone);
            }}
          />
        );
      })}
    </Svg>
  );
};

const PIN_W = 48;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  skinContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#FFF",
  },
  skin: {
    position: "absolute",
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  sketch: {
    flex: 1,
  },
  tools: {
    position: "absolute",
    top: 20,
    left: 20,
  },
  toolsRow: {
    flexDirection: "row",
    marginBottom: 10,
  },
  tool: {
    marginLeft: 10,
  },
  pins: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  pinOuter: {
    //position: "absolute",
    width: PIN_W,
    height: PIN_W,
  },
  pin: {
    position: "absolute",
    top: -PIN_W / 2,
    left: -PIN_W / 2,
    backgroundColor: "#000",
    width: PIN_W,
    height: PIN_W,
    borderRadius: PIN_W / 2,
    justifyContent: "center",
    alignItems: "center",
  },
  pinChar: {
    color: "#FFF",
  },
});

type ISkinZone = {
  name: ISkinZoneName;
  d: string;
  transform: string;
};

const svgZones: ISkinZone[] = [
  {
    name: "head",
    d:
      "M215.55,0s-18.71,6.8-24.38,32.31-31.75,23.25-39.69,19.85-29.48-11.22-35.71,2c0,0,2.27,12.7,14.17,14.4s17,0,17.58,9.07c0,0,30.49,34.58,61.17,10.2,0,0,34.64,28.35,65.82,4.54,0,0,.57-7.37,10.2-9.64a24.82,24.82,0,0,0,18.71-21s-11.9-11.34-28.34-5.11-29.49,0-34-15.3S234.82,1.7,215.55,0Z",
    transform: "translate(-1.82)",
  },
  {
    name: "throatLeft",
    d:
      "M146.38,84.47a52.94,52.94,0,0,0,35.15,17.46s-.56,44.34-11.34,71c0,0-27.21-9.63-43.65-7.37,0,0,19.28-31.18,13.61-56.12C140.15,109.42,146.38,107.72,146.38,84.47Z",
    transform: "translate(-1.82)",
  },
  {
    name: "neck",
    d:
      "M186.07,101.93s13.05,1.25,22.12-7.82c0,0,15.29,12.47,24.93,11.34,0,0-2.71,33.28,10.33,70.7,0,0-32.44,9.24-39.24,34.75,0,0-4.63-28.07-30-36.66C174.26,174.24,184.37,156.24,186.07,101.93Z",
    transform: "translate(-1.82)",
  },
  {
    name: "throatRight",
    d:
      "M274.93,99.59s-11.76,7.94-34.34,6.92c0,0-1.13,39.76,9.64,66.4,0,0,25.41-8.31,44.74-3.82,0,0-17.63-24.15-12-49.09C283,120,274.93,122.83,274.93,99.59Z",
    transform: "translate(-1.82)",
  },

  {
    name: "shoulderLeft",
    d:
      "M122,169.51s17.58-3.4,47.63,9.07,31.18,38,31.18,38-1.14,24.94-39.4,34.58S56.81,271.56,50,266.46L42.07,258,54,251.72,9.75,238.11S1.82,235.28,1.82,229,3,213.17,10.89,213.73,71,218.43,71,218.43l-3.4-9.8S103.66,209.93,122,169.51Z",
    transform: "translate(-1.82)",
  },
  {
    name: "thoraxLeft",
    d:
      "M55.67,272.69s117.36-6.23,137.77-34c0,0-19.85,56.12-24.95,91.84,0,0-39.12,19.84-92.41,9.64C76.08,340.16,71,281.76,55.67,272.69Z",
    transform: "translate(-1.82)",
  },
  {
    name: "sideLeft",
    d:
      "M75.52,345.64s51,10.2,91.84-8.32c0,0-14,48-14.36,103.94,0,0-56.7,2.65-89.42,20.87C63.58,462.13,82.7,429.92,75.52,345.64Z",
    transform: "translate(-1.82)",
  },
  {
    name: "legLeft",
    d:
      "M153.75,444.66s-65,5.67-93.73,23.43c0,0-11.72,14.37-12.47,21.17S48.3,502.11,43,507.4s-18.9,17.77-18.9,17.77S16.18,529.7,15,537.64s-6.8,16.63-10.2,18.52.75,4.15,2.64,4.91,19.66,14,27.6,2.27S48.3,537.64,60,534.24s28-12.1,30.24-14.74,21.16,15.87,32.12,16.25,26.08,2.27,45-4.16L181,530.46S149.18,509.48,153.75,444.66Z",
    transform: "translate(-1.82)",
  },
  {
    name: "shoulderRight",
    d:
      "M296.9,172.91s-26.08-3-56.12,9.5-31.19,38-31.19,38,1.14,25,39.41,34.58,104.59,20.41,111.4,15.31l7.94-8.5-11.91-6.24,44.22-13.6s7.94-2.84,7.94-9.08-1.14-15.87-9.07-15.3-60.1,4.69-60.1,4.69l3.4-9.79S315.24,213.33,296.9,172.91Z",
    transform: "translate(-1.82)",
  },
  {
    name: "thoraxRight",
    d:
      "M354.73,276.52s-117.36-6.24-137.76-34c0,0,19.84,56.13,24.94,91.84,0,0,39.12,19.85,92.41,9.64C334.32,344,339.42,285.59,354.73,276.52Z",
    transform: "translate(-1.82)",
  },
  {
    name: "sideRight",
    d:
      "M334.89,349.46s-51,10.21-91.85-8.31c0,0,14,48,14.37,103.94,0,0,57.21,1.79,89.15,20.29C346.56,465.38,327.71,433.75,334.89,349.46Z",
    transform: "translate(-1.82)",
  },
  {
    name: "legRight",
    d:
      "M256.65,448.49s65,5.67,93.73,23.43c0,0,11.72,14.36,12.47,21.17s-.75,12.85,4.54,18.14S386.29,529,386.29,529s7.93,4.54,9.07,12.47,6.8,16.63,10.2,18.52-.75,4.16-2.64,4.92-19.66,14-27.59,2.26-13.23-25.7-24.95-29.1-28-12.09-30.23-14.74S299,539.2,288,539.57s-26.08,2.27-45-4.15l-13.6-1.14S261.22,513.31,256.65,448.49Z",
    transform: "translate(-1.82)",
  },
  {
    name: "back",
    d:
      "M204.29,220.84S164.52,353.57,161.12,401s-10.77,94.49,25.51,131.34c0,0,9.08,8.5,10.78,21.54s10.77,26.65,14.74,0c0,0,.56-14.17,8.5-19.84s30.3-33.34,30.61-83.34c.33-51.59-25.37-148.92-46.91-229.64Z",
    transform: "translate(-1.82)",
  },
];

export default Skin;
