import * as React from "react";
import {
  Image,
  ImageStyle,
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableHighlight,
  View,
  ViewStyle,
} from "react-native";

interface IProps {
  label: string;
  buttonStyle?: StyleProp<ViewStyle>;
  labelStyle?: StyleProp<TextStyle>;
}

interface Styles {
  button: ViewStyle;
  icon: ImageStyle;
  label: TextStyle;
}

const styles = StyleSheet.create<Styles>({
  button: {
    flexDirection: "row",
    backgroundColor: "#336699",
    padding: 10,
  },

  icon: {
    width: 16,
    height: 16,
  },

  label: {
    color: "#F8F8F8",
    textAlign: "center",
  },
});

const Button = (props: any) => (
  <TouchableHighlight onPress={props.onPress}>
    <View style={[styles.button, props.buttonStyle]}>
      <Text style={[styles.label, props.labelStyle]}>{props.label}</Text>
    </View>
  </TouchableHighlight>
);

export default Button;
