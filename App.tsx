import { Path } from "@terrylinla/react-native-sketch-canvas";
import React, { useRef, useState } from "react";
import { StyleSheet, View } from "react-native";
import * as MediaLibrary from "expo-media-library";
import * as FileSystem from "expo-file-system";
import { zip, subscribe } from "react-native-zip-archive";
import Button from "./Button";
import Skin, { IPin } from "./Skin";
import { hasPerms } from "./utils";

const albumName = "report_num_visit_date";

export default function App() {
  const skin = useRef<any>(null);
  const [curSkin, setCurSkin] = useState(0);

  const [skins, setSkins] = useState<{ paths: Path[]; pins: IPin[] }[]>([
    { paths: [], pins: [] },
    { paths: [], pins: [] },
  ]);

  const doZip = async () => {
    console.log("doZip");
    //const uri = await zip(sourcePath, targetPath)
    const hasPerm = await hasPerms();
    if (!hasPerm) {
      return;
    }
    let startTime = Date.now();
    const album = await MediaLibrary.getAlbumAsync(albumName);
    const { assets } = await MediaLibrary.getAssetsAsync({
      album: album,
    });
    const dirPath = FileSystem.documentDirectory + albumName;
    await FileSystem.deleteAsync(dirPath, {
      idempotent: true,
    });
    await FileSystem.makeDirectoryAsync(dirPath, {
      intermediates: true,
    });
    for (let i = 0; i < assets.length; i++) {
      const asset = assets[i];
      const dest = `${dirPath}/${asset.filename}`;
      await FileSystem.copyAsync({
        from: asset.uri,
        to: dest,
      });
    }
    const zipProgress = subscribe(({ progress, filePath }) => {
      // the filePath is always empty on iOS for zipping.
      console.log(`progress: ${progress}\nprocessed at: ${filePath}`);
    });
    const zipUri = await zip(dirPath, `${dirPath}/${albumName}.zip`);
    const zipAsset = await MediaLibrary.createAssetAsync(zipUri);
    const zipName = `zip_${startTime}`;
    let zipAlbum = await MediaLibrary.getAlbumAsync(zipName);
    if (!zipAlbum) {
      zipAlbum = await MediaLibrary.createAlbumAsync(zipName, zipAsset);
    } else {
      await MediaLibrary.addAssetsToAlbumAsync([zipAsset], zipAlbum);
    }
    zipProgress.remove();
    console.log("ZIP SAVED", Date.now() - startTime);
  };

  const renderSkin = (index: number) => {
    const props: any = {};
    if (index) {
      props.name = `skin-${index}`;
    }
    return (
      <View style={styles.skinContainer}>
        <Skin
          ref={skin}
          {...props}
          paths={[...skins[index].paths]}
          pins={[...skins[index].pins]}
          onPathsChange={(paths) => {
            skins[index] = {
              ...skins[index],
              paths: [...paths],
            };
            setSkins([...skins]);
          }}
          newPinType="perforation"
          onPinsChange={(pins) => {
            skins[index] = {
              ...skins[index],
              pins: [...pins],
            };
            setSkins([...skins]);
          }}
        />
      </View>
    );
  };

  console.log("Render App");

  return (
    <View style={styles.container}>
      <View style={styles.buttons}>
        <Button
          label="1"
          onPress={() => setCurSkin(0)}
          buttonStyle={{
            backgroundColor: curSkin == 0 ? "#336699" : "#CCC",
          }}
        />
        <Button
          label="2"
          onPress={() => setCurSkin(1)}
          buttonStyle={{
            backgroundColor: curSkin == 1 ? "#336699" : "#CCC",
          }}
        />
        <Button
          label="save"
          onPress={() => {
            skin.current.capture(albumName);
          }}
        />
        <Button
          label="zip"
          onPress={() => {
            doZip();
          }}
        />
      </View>
      {curSkin == 0 && renderSkin(0)}
      {curSkin == 1 && renderSkin(1)}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "red",
  },
  skinContainer: {
    position: "relative",
    flex: 1,
  },
  buttons: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
  },
});
