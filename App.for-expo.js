import Expo from "expo";
import * as ExpoPixi from "expo-pixi";
import React, { Component } from "react";
import {
  ImageBackground,
  Button,
  Platform,
  AppState,
  StyleSheet,
  View,
} from "react-native";
import { captureRef } from "react-native-view-shot";
import * as MediaLibrary from "expo-media-library";
import * as FileSystem from "expo-file-system";
import JSZipUtils from "jszip-utils";
import JSZip from "jszip";

const isAndroid = Platform.OS === "android";
function uuidv4() {
  //https://stackoverflow.com/a/2117523/4047926
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

const albumName = "report_num_visit_date";

export default class App extends Component {
  state = {
    image: null,
    strokeColor: Math.random() * 0xffffff,
    strokeWidth: Math.random() * 30 + 10,
    lines: [
      {
        points: [
          { x: 300, y: 300 },
          { x: 600, y: 300 },
          { x: 450, y: 600 },
          { x: 300, y: 300 },
        ],
        color: 0xff00ff,
        alpha: 1,
        width: 10,
      },
    ],
    appState: AppState.currentState,
  };

  handleAppStateChangeAsync = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      if (isAndroid && this.sketch) {
        this.setState({
          appState: nextAppState,
          id: uuidv4(),
          lines: this.sketch.lines,
        });
        return;
      }
    }
    this.setState({ appState: nextAppState });
  };

  componentDidMount() {
    AppState.addEventListener("change", this.handleAppStateChangeAsync);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handleAppStateChangeAsync);
  }

  hasPerms = async () => {
    let perm = await MediaLibrary.getPermissionsAsync();
    if (!perm.granted) {
      perm = await MediaLibrary.requestPermissionsAsync();
    }
    return perm.granted;
  };

  onChangeAsync = async () => {
    const tmp = await captureRef(this.containerToCapture, {
      result: "tmpfile",
      /* height: pixels,
      width: pixels, */
      quality: 1,
      format: "jpg",
    });

    this.setState({
      image: { uri: tmp },
      strokeWidth: Math.random() * 30 + 10,
      strokeColor: Math.random() * 0xffffff,
    });

    const dest = `${FileSystem.documentDirectory}${new Date().getTime()}.jpg`;
    await FileSystem.moveAsync({
      from: tmp,
      to: dest,
    });
    const { uri } = await FileSystem.getInfoAsync(dest);

    const hasPerm = await this.hasPerms();
    if (!hasPerm) {
      return;
    }

    const asset = await MediaLibrary.createAssetAsync(uri);
    let album = await MediaLibrary.getAlbumAsync(albumName);
    if (!album) {
      MediaLibrary.createAlbumAsync(albumName, asset, false);
    } else {
      MediaLibrary.addAssetsToAlbumAsync([asset], album, false);
    }
  };

  onZipClick = async () => {
    const hasPerm = await this.hasPerms();
    if (!hasPerm) {
      return;
    }
    let startTime = Date.now();
    const album = await MediaLibrary.getAlbumAsync(albumName);
    const { assets } = await MediaLibrary.getAssetsAsync({
      album: album,
    });
    const zipName = `zip_${startTime}`;
    let zipAlbum = await MediaLibrary.getAlbumAsync(zipName);
    const zip = new JSZip();
    for (let i = 0; i < assets.length; i++) {
      const now = Date.now();
      const asset = assets[i];
      const str = await FileSystem.readAsStringAsync(asset.uri, {
        encoding: FileSystem.EncodingType.Base64,
      });
      await zip.file(asset.filename, str, {
        base64: true,
      });
      console.log("Added to zip", Date.now() - now);
      //const res = await MediaLibrary.addAssetsToAlbumAsync([asset2], album);
      //await MediaLibrary.saveToLibraryAsync(asset2.uri);
    }
    console.log("All added to zip", Date.now() - startTime);

    setTimeout(async () => {
      let now = Date.now();
      //Memory leaks
      const zipStr = await zip.generateAsync({ type: "base64" });
      // Not supported on this platform
      /* zip.generateNodeStream({ streamFiles: true }).on("data", (e) => {
        console.log(e);
      }); */
      console.log("Zip generated", Date.now() - now);
      //now = Date.now();
      const path = `${FileSystem.documentDirectory}${new Date().getTime()}.zip`;
      await FileSystem.writeAsStringAsync(path, zipStr, {
        encoding: FileSystem.EncodingType.Base64,
      });
      //console.log("Zip as string", Date.now() - now);
      //now = Date.now();
      const { uri: zipUri } = await FileSystem.getInfoAsync(path);
      const zipAsset = await MediaLibrary.createAssetAsync(zipUri);
      if (!zipAlbum) {
        zipAlbum = await MediaLibrary.createAlbumAsync(zipName, zipAsset);
      } else {
        await MediaLibrary.addAssetsToAlbumAsync([zipAsset], zipAlbum);
      }
      //console.log("Zip SAVED", Date.now() - now);
      console.log("Total", Date.now() - startTime);
    }, 100);
  };

  onReady = () => {
    console.log("ready!");
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <View style={styles.sketchContainer}>
            <ImageBackground
              ref={(ref) => (this.containerToCapture = ref)}
              source={require("./assets/leather.png")}
              style={styles.imageBg}
            >
              <ExpoPixi.Sketch
                ref={(ref) => (this.sketch = ref)}
                style={styles.sketch}
                strokeColor={this.state.strokeColor}
                strokeWidth={this.state.strokeWidth}
                strokeAlpha={1}
                //onChange={this.onChangeAsync}
                onReady={this.onReady}
                initialLines={this.state.lines}
              />
            </ImageBackground>
          </View>
        </View>
        <View style={styles.buttons}>
          <Button
            color={"blue"}
            title="undo"
            style={styles.button}
            onPress={() => {
              this.sketch.undo();
            }}
          />
          <Button
            color={"red"}
            title="save"
            style={styles.button}
            onPress={() => {
              this.onChangeAsync();
            }}
          />
          <Button
            color={"red"}
            title="zip"
            style={styles.button}
            onPress={() => {
              this.onZipClick();
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  sketch: {
    flex: 1,
    backgroundColor: "transparent",
  },
  sketchContainer: {
    height: "100%",
  },
  image: {
    flex: 1,
  },
  imageBg: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    backgroundColor: "white",
  },
  buttons: {
    flexDirection: "row",
    backgroundColor: "green",
    justifyContent: "space-around",
  },
  button: {
    // position: 'absolute',
    // bottom: 8,
    // left: 8,
    zIndex: 1,
    padding: 12,
    paddingHorizontal: 40,
    minHeight: 60,
  },
});
