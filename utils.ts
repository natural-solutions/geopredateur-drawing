import * as MediaLibrary from "expo-media-library";

export const hasPerms = async () => {
  let perm = await MediaLibrary.getPermissionsAsync();
  if (!perm.granted) {
    perm = await MediaLibrary.requestPermissionsAsync();
  }
  return perm.granted;
};
